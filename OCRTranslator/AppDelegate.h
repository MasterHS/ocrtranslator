//
//  AppDelegate.h
//  OCRTranslator
//
//  Created by Hazem Safetli on 02/08/14.
//  Copyright (c) 2014 Hazem Safetli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

