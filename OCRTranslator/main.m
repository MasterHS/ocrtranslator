//
//  main.m
//  OCRTranslator
//
//  Created by Hazem Safetli on 02/08/14.
//  Copyright (c) 2014 Hazem Safetli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
